function [  ] = mandelplot( FractCell,how_many_images, depth, nloop, fps)
%% Plotting function tailored to the mandelbrot script.
Fractalmov           = VideoWriter('Nomalos_1.avi','Archival');         % Creates the skeleton for an .avi file
Fractalmov.FrameRate = fps;                                  % To match up with the frame rate in the movie
Fractalmov.open
for m = 1:how_many_images
  %figure(m);                      % Omitted for plot-time. Remove '%' if any frame seems interesting
  image(FractCell{m});             % A plot of each and every fractal
  axis image;
  colormap(flipud(jet(depth)));    % Create a colourmap based on the depth
                                   % Different colormaps: gray, hot, cool, bone, copper, pink, flag, prism, jet
  fractmov(m) = getframe(gcf);     % Store all the fractal images in one object
  if how_many_images ~= 1            
    writeVideo(Fractalmov, fractmov(m)); % Creates a movie, frame by frame
  end
end
if how_many_images ~= 1            % Only play movie if there's a movie
    movie(gcf,fractmov,nloop,fps)  % Plays the movie just created in MATLAB
    fprintf('Fractalmov is saved to Current Folder \n')
end